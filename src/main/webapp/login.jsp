<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body class="text-center">
        <form class="form-signin" method="post" action="user">
            <img class="mb-4" src="img/logo.jpg" alt="logo" width="75%" height="75%">
            <h3>Log in</h3>
            <label for="inputUsername">Email:</label>
            <input type="email" name="username" id="inputUsername" class="form-control" placeholder="Email" required autofocus maxlength="50" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
            <br>
            <label for="inputPassword">Password:</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <%
                if (request.getParameter("error") != null) {
                    out.println("<p class='error'>Invalid username and/or password</p>");
                }
            %>
            <c:if test="${error != null}">
                <p class='error'>${error}</p>
            </c:if>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="action" value="login">Log in</button>
        </form>
    </body>
</html>
