<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="html/adminMenu.html"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>    
        <!-- List all doctors with modify and delete buttons -->
        <c:if test="${doctors != null}">
            <h3>DOCTOR LIST</h3>
            <form action="doctorController" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telephone</th>

                            <c:if test="${param.actionDoctor == 'listDoctorsToModify'}">
                                <th scope="col">Modify doctor</th>
                                </c:if>

                            <c:if test="${param.actionDoctor == 'listDoctorsToDelete'}">
                                <th scope="col">Delete doctor</th>
                                </c:if>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${doctors}" var="doctor">
                            <tr>
                                <th scope="row">${doctor.idDoctor}</th>
                                <td>${doctor.name}</td>
                                <td>${doctor.surname}</td>
                                <td>${doctor.email}</td>
                                <td>${doctor.telephone}</td>
                                <c:if test="${param.actionDoctor == 'listDoctorsToModify'}">
                                    <td scope="col">
                                        <button class="btn btn-success" type="submit" value="${doctor.idDoctor};${doctor.name};${doctor.surname};${doctor.email};${doctor.telephone}" name="doctor">Modify</button>
                                        <input type="hidden" name="actionDoctor" value="modifyDoctorForm"/>
                                    </td>
                                </c:if>
                                <c:if test="${param.actionDoctor == 'listDoctorsToDelete'}">
                                    <td scope="col">
                                        <button class="btn btn-danger" type="submit" value="${doctor.idDoctor};${doctor.name}" name="doctor">Delete</button>
                                        <input type="hidden" name="actionDoctor" value="deleteDoctor"/>
                                    </td>                                        
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </form>
        </c:if>

        <!-- Add new doctor -->
        <c:if test="${param.showAddDoctorForm != null}" >
            <jsp:include page = "addDoctorForm.jsp"/>
        </c:if>

        <!-- Modify doctor -->
        <c:if test="${doctormodify != null}">
            <jsp:include page = "modifyDoctorForm.jsp"/>
        </c:if>

        <!-- List all patients with modify and delete buttons -->
        <c:if test="${patients != null}">
            <h3>PATIENT LIST</h3>
            <form action="patientController" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Age</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">Doctor</th>

                            <c:if test="${param.actionPatient == 'listPatientsToModify'}">
                                <th scope="col">Modify patient</th>
                                </c:if>

                            <c:if test="${param.actionPatient == 'listPatientsToDelete'}">
                                <th scope="col">Delete patient</th>
                                </c:if>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${patients}" var="patient">
                            <tr>
                                <th scope="row">${patient.idPatient}</th>
                                <td>${patient.name}</td>
                                <td>${patient.surname}</td>
                                <td>${patient.dni}</td>
                                <td>${patient.age}</td>
                                <td>${patient.email}</td>
                                <td>${patient.telephone}</td>
                                <td>${patient.idDoctor}</td>
                                <c:if test="${param.actionPatient == 'listPatientsToModify'}">
                                    <td scope="col">
                                        <button class="btn btn-success" type="submit" value="${patient.idPatient};${patient.name};${patient.surname};${patient.dni};${patient.age};${patient.email};${patient.telephone};${patient.idDoctor}" name="patient">Modify</button>
                                        <input type="hidden" name="actionPatient" value="modifyPatientForm"/>
                                    </td>
                                </c:if>
                                <c:if test="${param.actionPatient == 'listPatientsToDelete'}">
                                    <td scope="col">
                                        <button class="btn btn-danger" type="submit" value="${patient.idPatient};${patient.name}" name="patient">Delete</button>
                                        <input type="hidden" name="actionPatient" value="deletePatient"/>
                                    </td>                                        
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </form>
        </c:if>

        <!-- Add new patient -->
        <c:if test="${param.showAddPatientForm != null}" >
            <jsp:include page = "addPatientFormA.jsp"/> 
        </c:if>

        <!-- Modify patient -->
        <c:if test="${patientmodify != null}">
            <jsp:include page = "modifyPatientFormA.jsp"/>
        </c:if>

        <!-- Success message -->
        <c:if test="${success != null}">
            <div class="alert alert-success" role="alert">${success}</div>
        </c:if>

        <!-- Error message -->
        <c:if test="${danger != null}">
            <div class="alert alert-danger" role="alert">${error}</div>
        </c:if>
    </body>
</html>

<jsp:include page="html/footer.html"/>
