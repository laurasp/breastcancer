<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="html/patientMenu.html"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Patient</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <!-- List patient with view result button -->
        <c:if test="${patient != null}">
            <form action="patientController" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Age</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">Doctor</th>
                            <th scope="col">Result</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">${patient.idPatient}</th>
                            <td>${patient.name}</td>
                            <td>${patient.surname}</td>
                            <td>${patient.dni}</td>
                            <td>${patient.age}</td>
                            <td>${patient.email}</td>
                            <td>${patient.telephone}</td>
                            <td>${patient.idDoctor}</td>
                            <c:if test="${param.actionPatient == 'listPatientToViewResult'}">
                                <td scope="col">
                                    <button class="btn btn-success" type="submit" value="${patient.brca1Seq};${patient.brca2Seq}" name="patient">View results</button>
                                    <input type="hidden" name="actionPatient" value="viewResults"/>
                                </td>
                            </c:if>
                        </tr>
                    </tbody>
                </table>
            </form>
        </c:if>

        <!-- Success message -->
        <c:if test="${success != null}">
            <div class="alert alert-success" role="alert">
                <pre>${success}</pre>
            </div>
        </c:if>

        <!-- Error message -->
        <c:if test="${danger != null}">
            <div class="alert alert-danger" role="alert">${error}</div>
        </c:if>
    </body>
</html>

<jsp:include page="html/footer.html"/>
