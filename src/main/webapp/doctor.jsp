<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="html/doctorMenu.html"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Doctor</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <!-- List patients assigned to the logged doctor with modify and delete buttons -->
        <c:if test="${patients != null}">
            <h3>PATIENT LIST</h3>
            <form action="patientController" method="POST">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">DNI</th>
                            <th scope="col">Age</th>
                            <th scope="col">Email</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">Result</th>
                            <th scope="col">Doctor</th>

                            <c:if test="${param.actionPatient == 'listPatientsToModify'}">
                                <th scope="col">Modify patient</th>
                                </c:if>

                            <c:if test="${param.actionPatient == 'listPatientsToDelete'}">
                                <th scope="col">Delete patient</th>
                                </c:if>

                            <c:if test="${param.actionPatient == 'listPatientsToSequence'}">
                                <th scope="col">Analyse sequences</th>
                                </c:if>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${patients}" var="patient">
                            <tr>
                                <th scope="row">${patient.idPatient}</th>
                                <td>${patient.name}</td>
                                <td>${patient.surname}</td>
                                <td>${patient.dni}</td>
                                <td>${patient.age}</td>
                                <td>${patient.email}</td>
                                <td>${patient.telephone}</td>
                                <td>${patient.result}</td>
                                <td>${patient.idDoctor}</td>
                                <c:if test="${param.actionPatient == 'listPatientsToModify'}">
                                    <td scope="col">
                                        <button class="btn btn-success" type="submit" value="${patient.idPatient};${patient.name};${patient.surname};${patient.dni};${patient.age};${patient.email};${patient.telephone}" name="patient">Modify</button>
                                        <input type="hidden" name="actionPatient" value="modifyPatientForm"/>
                                    </td>
                                </c:if>
                                <c:if test="${param.actionPatient == 'listPatientsToDelete'}">
                                    <td scope="col">
                                        <button class="btn btn-danger" type="submit" value="${patient.idPatient};${patient.name}" name="patient">Delete</button>
                                        <input type="hidden" name="actionPatient" value="deletePatient"/>
                                    </td>                                        
                                </c:if>
                                <c:if test="${param.actionPatient == 'listPatientsToSequence'}">
                                    <td scope="col">
                                        <button class="btn btn-primary" type="submit" value="${patient.idPatient};${patient.brca1Seq};${patient.brca2Seq}" name="patient">Analyse sequences & Update result</button>
                                        <input type="hidden" name="actionPatient" value="analyseSequencesAndUpdateResult"/>
                                    </td>                                      
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </form>
        </c:if>

        <!-- Add new patient -->
        <c:if test="${param.showAddPatientForm != null}" >
            <jsp:include page = "addPatientFormD.jsp"/> 
        </c:if>

        <!-- Modify patient -->
        <c:if test="${patientmodify != null}">
            <jsp:include page = "modifyPatientFormD.jsp"/>
        </c:if>

        <!-- Success message -->
        <c:if test="${success != null}">
            <div class="alert alert-success" role="alert">
                <pre>${success}</pre>
            </div>
        </c:if>

        <!-- Error message -->
        <c:if test="${danger != null}">
            <div class="alert alert-danger" role="alert">${error}</div>
        </c:if>
    </body>
</html>

<jsp:include page="html/footer.html"/>
