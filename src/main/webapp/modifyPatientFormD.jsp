<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="html/doctorMenu.html"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin/Doctor</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <form action="patientController" method="POST">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPatient">Name:</label>
                    <input type="text" class="form-control" id="inputPatient" value="${patientmodify.name}" name="patName" required maxlength="30">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPatient">Surname:</label>
                    <input type="text" class="form-control" id="inputPatient" value="${patientmodify.surname}" name="patSurname" required maxlength="30">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPatient">DNI:</label>
                    <input type="text" class="form-control" id="inputPatient" value="${patientmodify.dni}" name="patDni" required maxlength="9" pattern="^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$">
                    <%
                        if (request.getParameter("errorDni") != null) {
                            out.println("<p class='error'>DNI already exists in database</p>");
                        }
                    %>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPatient">Age:</label>
                    <input type="number" class="form-control" id="inputPatient" value="${patientmodify.age}" name="patAge" required min="1" max="150">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Email:</label>
                    <input type="email" class="form-control" id="inputPatient" value="${patientmodify.email}" name="patEmail" required maxlength="50" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                    <%
                        if (request.getParameter("errorEmail") != null) {
                            out.println("<p class='error'>Email already exists in database</p>");
                        }
                    %>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Telephone:</label>
                    <input type="text" class="form-control" id="inputPatient" value="${patientmodify.telephone}" name="patTelephone" required maxlength="20">
                    <%
                        if (request.getParameter("errorTelephone") != null) {
                            out.println("<p class='error'>Telephone already exists in database</p>");
                        }
                    %>
                </div>
            </div>
            <input type="hidden" value="${patientmodify.idPatient}" name="patId"/>
            <button type="submit" class="btn btn-primary" value="modifyPatientDoctor" name="actionPatient">Modify patient</button>
        </form>
    </body>
</html>

<jsp:include page="html/footer.html"/>
