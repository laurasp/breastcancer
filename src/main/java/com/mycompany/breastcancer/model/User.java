package com.mycompany.breastcancer.model;

public enum User {
    PATIENT, DOCTOR, ADMIN;
}
