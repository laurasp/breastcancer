package com.mycompany.breastcancer.model.persist;

import com.mycompany.breastcancer.model.Doctor;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author tarda
 */
public class DoctorDAO {

    private static DBConnect dataSource;
    private final Properties queries;
    private static String PROPS_FILE;

    public DoctorDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/doctor_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));
        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    /**
     * It retrieves all the doctors from the database
     *
     * @return
     */
    public ArrayList<Doctor> getAllDoctors() {
        ArrayList<Doctor> doctors = new ArrayList<>();

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_ALL"));) {
            ResultSet res = pst.executeQuery();

            while (res.next()) {
                Doctor doc = new Doctor();
                doc.setIdDoctor(res.getInt("idDoctor"));
                doc.setName(res.getString("name"));
                doc.setSurname(res.getString("surname"));
                doc.setEmail(res.getString("email"));
                doc.setTelephone(res.getString("telephone"));
                doctors.add(doc);
            }

        } catch (SQLException e) {
            doctors = new ArrayList<>();
        }

        return doctors;
    }

    /**
     * It adds a new doctor to the database
     *
     * @param doc
     * @return
     */
    public int addNewDoctor(Doctor doc) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("INSERT"));) {
            pst.setString(1, doc.getName());
            pst.setString(2, doc.getSurname());
            pst.setString(3, doc.getEmail());
            pst.setString(4, doc.getTelephone());
            pst.setString(5, doc.getPassword());
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It modifies a doctor from the database
     *
     * @param doc
     * @return
     */
    public int modifyDoctor(Doctor doc) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("UPDATE"));) {
            pst.setString(1, doc.getName());
            pst.setString(2, doc.getSurname());
            pst.setString(3, doc.getEmail());
            pst.setString(4, doc.getTelephone());
            pst.setInt(5, doc.getIdDoctor());
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It deletes a doctor from the database given the email
     *
     * @param idDoctor
     * @return
     */
    public int deleteDoctor(int idDoctor) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("DELETE"));) {
            pst.setInt(1, idDoctor);
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It retrieves a doctor from the database given the email
     *
     * @param email
     * @return
     */
    public Doctor getDoctorByEmail(String email) {
        Doctor doc;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_EMAIL"));) {

            pst.setString(1, email);
            ResultSet res = pst.executeQuery();
            res.next();
            doc = new Doctor(res.getInt("idDoctor"), res.getString("name"), res.getString("surname"), res.getString("email"), res.getString("telephone"), res.getString("password"));

        } catch (SQLException e) {
            doc = null;
        }

        return doc;
    }

    /**
     * It retrieves a doctor from the database given the telephone
     *
     * @param telephone
     * @return
     */
    public Doctor getDoctorByTelephone(String telephone) {
        Doctor doc = new Doctor();

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_TELEPHONE"));) {

            pst.setString(1, telephone);
            ResultSet res = pst.executeQuery();
            res.next();
            doc.setTelephone(res.getString("telephone"));

        } catch (SQLException e) {
            doc = null;
        }

        return doc;
    }
}
