package com.mycompany.breastcancer.model.persist;

import com.mycompany.breastcancer.model.Patient;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import org.biojava.nbio.alignment.NeedlemanWunsch;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.alignment.template.GapPenalty;
import org.biojava.nbio.core.alignment.matrices.SubstitutionMatrixHelper;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;

/**
 *
 * @author tarda
 */
public class PatientDAO {

    private final Properties queries;
    private static String PROPS_FILE;
    private static DBConnect dataSource;

    public PatientDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/patient_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));
        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    /**
     * <strong>getDataSource()</strong>
     *
     * @return object to connect to database.
     */
    public static DBConnect getDataSource() {
        return dataSource;
    }

    /**
     * It retrieves all the patients from the database
     *
     * @return
     */
    public ArrayList<Patient> getAllPatients() {
        ArrayList<Patient> patients = new ArrayList<>();

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_ALL_A"));) {
            ResultSet res = pst.executeQuery();

            while (res.next()) {
                Patient pat = new Patient();
                pat.setIdPatient(res.getInt("idPatient"));
                pat.setName(res.getString("name"));
                pat.setSurname(res.getString("surname"));
                pat.setDni(res.getString("dni"));
                pat.setAge(res.getInt("age"));
                pat.setEmail(res.getString("email"));
                pat.setTelephone(res.getString("telephone"));
                pat.setResult(res.getDouble("result"));
                pat.setIdDoctor(res.getInt("idDoctor"));
                patients.add(pat);
            }
        } catch (SQLException e) {
            patients = new ArrayList<>();
        }

        return patients;
    }

    /**
     * It retrieves the patients from the database assigned to the logged doctor
     *
     * @param email
     * @return
     */
    public ArrayList<Patient> getPatientsByDoctor(String email) {
        ArrayList<Patient> patients = new ArrayList<>();

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_ALL_D"));) {
            pst.setString(1, email);

            ResultSet res = pst.executeQuery();

            while (res.next()) {
                Patient pat = new Patient();
                pat.setIdPatient(res.getInt("idPatient"));
                pat.setName(res.getString("name"));
                pat.setSurname(res.getString("surname"));
                pat.setDni(res.getString("dni"));
                pat.setAge(res.getInt("age"));
                pat.setEmail(res.getString("email"));
                pat.setTelephone(res.getString("telephone"));
                pat.setBrca1Seq(res.getString("brca1Seq"));
                pat.setBrca2Seq(res.getString("brca2Seq"));
                pat.setResult(res.getDouble("result"));
                pat.setIdDoctor(res.getInt("idDoctor"));
                patients.add(pat);
            }
        } catch (SQLException e) {
            patients = new ArrayList<>();
        }

        return patients;
    }

    /**
     * It adds a new patient to the database
     *
     * @param pat
     * @return
     */
    public int addNewPatient(Patient pat) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("INSERT"));) {
            pst.setString(1, pat.getName());
            pst.setString(2, pat.getSurname());
            pst.setString(3, pat.getDni());
            pst.setInt(4, pat.getAge());
            pst.setString(5, pat.getEmail());
            pst.setString(6, pat.getTelephone());
            pst.setString(7, pat.getPassword());
            pst.setString(8, pat.getBrca1Seq());
            pst.setString(9, pat.getBrca2Seq());
            pst.setDouble(10, pat.getResult());
            pst.setInt(11, pat.getIdDoctor());
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It modifies a patient from the database. This function is only for
     * admins, as they can assign a doctor to the patient
     *
     * @param pat
     * @return
     */
    public int modifyPatientA(Patient pat) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("UPDATE_A"));) {
            pst.setString(1, pat.getName());
            pst.setString(2, pat.getSurname());
            pst.setString(3, pat.getDni());
            pst.setInt(4, pat.getAge());
            pst.setString(5, pat.getEmail());
            pst.setString(6, pat.getTelephone());
            pst.setInt(7, pat.getIdDoctor());
            pst.setInt(8, pat.getIdPatient());
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It modifies a patient from the database. This function is only for
     * doctors. The doctor assigned to the patient will be the logged doctor
     *
     * @param pat
     * @return
     */
    public int modifyPatientD(Patient pat) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("UPDATE_D"));) {
            pst.setString(1, pat.getName());
            pst.setString(2, pat.getSurname());
            pst.setString(3, pat.getDni());
            pst.setInt(4, pat.getAge());
            pst.setString(5, pat.getEmail());
            pst.setString(6, pat.getTelephone());
            pst.setInt(7, pat.getIdPatient());
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It deletes a patient from the database
     *
     * @param idPatient
     * @return
     */
    public int deletePatient(int idPatient) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("DELETE"));) {
            pst.setInt(1, idPatient);
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }

    /**
     * It retrieves a patient from the database given the email
     *
     * @param email
     * @return
     */
    public Patient getPatientByEmail(String email) {
        Patient pat;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_EMAIL"));) {

            pst.setString(1, email);
            ResultSet res = pst.executeQuery();
            res.next();
            pat = new Patient(res.getInt("idPatient"), res.getString("name"), res.getString("surname"), res.getString("dni"), res.getInt("age"), res.getString("email"), res.getString("telephone"), res.getString("password"), res.getString("brca1Seq"), res.getString("brca2Seq"), res.getDouble("result"), res.getInt("idDoctor"));
        } catch (SQLException e) {
            pat = null;
        }

        return pat;
    }

    /**
     * It retrieves a patient from the database given the telephone
     *
     * @param telephone
     * @return
     */
    public Patient getPatientByTelephone(String telephone) {
        Patient pat;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_TELEPHONE"));) {

            pst.setString(1, telephone);
            ResultSet res = pst.executeQuery();
            res.next();
            pat = new Patient(res.getInt("idPatient"), res.getString("name"), res.getString("surname"), res.getString("dni"), res.getInt("age"), res.getString("email"), res.getString("telephone"), res.getString("password"), res.getString("brca1Seq"), res.getString("brca2Seq"), res.getDouble("result"), res.getInt("idDoctor"));

        } catch (SQLException e) {
            pat = null;
        }

        return pat;
    }

    /**
     * It retrieves a patient from the database given the DNI
     *
     * @param dni
     * @return
     */
    public Patient getPatientByDni(String dni) {
        Patient pat;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_DNI"));) {

            pst.setString(1, dni);
            ResultSet res = pst.executeQuery();
            res.next();
            pat = new Patient(res.getInt("idPatient"), res.getString("name"), res.getString("surname"), res.getString("dni"), res.getInt("age"), res.getString("email"), res.getString("telephone"), res.getString("password"), res.getString("brca1Seq"), res.getString("brca2Seq"), res.getDouble("result"), res.getInt("idDoctor"));

        } catch (SQLException e) {
            pat = null;
        }

        return pat;
    }

    /**
     * It analyses the BRCA1 sequence from the patient
     *
     * @param brca1P
     * @return
     * @throws CompoundNotFoundException
     */
    public NeedlemanWunsch analyseBrca1(String brca1P) throws CompoundNotFoundException {
        //String uniprotIDBrca1 = "P38398";
        //ProteinSequence brca1Uniprot = getSequenceForId(uniprotIDBrca1);
        //length = 1,863
        ProteinSequence brca1Pat = new ProteinSequence(brca1P);

        ProteinSequence brca1Uniprot = new ProteinSequence(
                "MDLSALRVEEVQNVINAMQKILECPICLELIKEPVSTKCDHIFCKFCMLKLLNQKKGPSQ"
                + "CPLCKNDITKRSLQESTRFSQLVEELLKIICAFQLDTGLEYANSYNFAKKENNSPEHLKD"
                + "EVSIIQSMGYRNRAKRLLQSEPENPSLQETSLSVQLSNLGTVRTLRTKQRIQPQKTSVYI"
                + "ELGSDSSEDTVNKATYCSVGDQELLQITPQGTRDEISLDSAKKAACEFSETDVTNTEHHQ"
                + "PSNNDLNTTEKRAAERHPEKYQGSSVSNLHVEPCGTNTHASSLQHENSSLLLTKDRMNVE"
                + "KAEFCNKSKQPGLARSQHNRWAGSKETCNDRRTPSTEKKVDLNADPLCERKEWNKQKLPC"
                + "SENPRDTEDVPWITLNSSIQKVNEWFSRSDELLGSDDSHDGESESNAKVADVLDVLNEVD"
                + "EYSGSSEKIDLLASDPHEALICKSERVHSKSVESNIEDKIFGKTYRKKASLPNLSHVTEN"
                + "LIIGAFVTEPQIIQERPLTNKLKRKRRPTSGLHPEDFIKKADLAVQKTPEMINQGTNQTE"
                + "QNGQVMNITNSGHENKTKGDSIQNEKNPNPIESLEKESAFKTKAEPISSSISNMELELNI"
                + "HNSKAPKKNRLRRKSSTRHIHALELVVSRNLSPPNCTELQIDSCSSSEEIKKKKYNQMPV"
                + "RHSRNLQLMEGKEPATGAKKSNKPNEQTSKRHDSDTFPELKLTNAPGSFTKCSNTSELKE"
                + "FVNPSLPREEKEEKLETVKVSNNAEDPKDLMLSGERVLQTERSVESSSISLVPGTDYGTQ"
                + "ESISLLEVSTLGKAKTEPNKCVSQCAAFENPKGLIHGCSKDNRNDTEGFKYPLGHEVNHS"
                + "RETSIEMEESELDAQYLQNTFKVSKRQSFAPFSNPGNAEEECATFSAHSGSLKKQSPKVT"
                + "FECEQKEENQGKNESNIKPVQTVNITAGFPVVGQKDKPVDNAKCSIKGGSRFCLSSQFRG"
                + "NETGLITPNKHGLLQNPYRIPPLFPIKSFVKTKCKKNLLEENFEEHSMSPEREMGNENIP"
                + "STVSTISRNNIRENVFKEASSSNINEVGSSTNEVGSSINEIGSSDENIQAELGRNRGPKL"
                + "NAMLRLGVLQPEVYKQSLPGSNCKHPEIKKQEYEEVVQTVNTDFSPYLISDNLEQPMGSS"
                + "HASQVCSETPDDLLDDGEIKEDTSFAENDIKESSAVFSKSVQKGELSRSPSPFTHTHLAQ"
                + "GYRRGAKKLESSEENLSSEDEELPCFQHLLFGKVNNIPSQSTRHSTVATECLSKNTEENL"
                + "LSLKNSLNDCSNQVILAKASQEHHLSEETKCSASLFSSQCSELEDLTANTNTQDPFLIGS"
                + "SKQMRHQSESQGVGLSDKELVSDDEERGTGLEENNQEEQSMDSNLGEAASGCESETSVSE"
                + "DCSGLSSQSDILTTQQRDTMQHNLIKLQQEMAELEAVLEQHGSQPSNSYPSIISDSSALE"
                + "DLRNPEQSTSEKAVLTSQKSSEYPISQNPEGLSADKFEVSADSSTSKNKEPGVERSSPSK"
                + "CPSLDDRWYMHSCSGSLQNRNYPSQEELIKVVDVEEQQLEESGPHDLTETSYLPRQDLEG"
                + "TPYLESGISLFSDDPESDPSEDRAPESARVGNIPSSTSALKVPQLKVAESAQSPAAAHTT"
                + "DTAGYNAMEESVSREKPELTASTERVNKRMSMVVSGLTPEEFMLVYKFARKHHITLTNLI"
                + "TEETTHVVMKTDAEFVCERTLKYFLGIAGGKWVVSYFWVTQSIKERKMLNEHDFEVRGDV"
                + "VNGRNHQGPKRARESQDRKIFRGLEICCYGPFTNMPTDQLEWMVQLCGASVVKELSSFTL"
                + "GTGVHPIVVVQPDAWTEDNGFHAIGQMCEAPVVTREWVLDSVALYQCQELDTYLIPQIPH"
                + "SHY");

        GapPenalty penalty = new SimpleGapPenalty();
        int gOpen = 8;
        int gExtend = 1;
        penalty.setOpenPenalty(gOpen);
        penalty.setExtensionPenalty(gExtend);

        SubstitutionMatrix<AminoAcidCompound> matrix = SubstitutionMatrixHelper.getBlosum62();

        NeedlemanWunsch nw = new NeedlemanWunsch(brca1Pat, brca1Uniprot, penalty, matrix);

        return nw;
    }

    /**
     * It analyses the BRCA2 sequence from the patient
     *
     * @param brca2P
     * @return
     * @throws CompoundNotFoundException
     */
    public NeedlemanWunsch analyseBrca2(String brca2P) throws CompoundNotFoundException {
        //String uniprotIDBrca2 = "P51587";
        //ProteinSequence brca2Prot = getSequenceForId(uniprotIDBrca2);
        //length = 3,418

        ProteinSequence brca2Pat = new ProteinSequence(brca2P);

        ProteinSequence brca2Uniprot = new ProteinSequence(
                "MPIGSKERPTFFEIFKTRCNKADLGPISLNWFEELSSEAPPYNSEPAEESEHKNNNYEPN"
                + "LFKTPQRKPSYNQLASTPIIFKEQGLTLPLYQSPVKELDKFKLDLGRNVPNSRHKSLRTV"
                + "KTKMDQADDVSCPLLNSCLSESPVVLQCTHVTPQRDKSVVCGSLFHTPKFVKGRQTPKHI"
                + "SESLGAEVDPDMSWSSSLATPPTLSSTVLIVRNEEASETVFPHDTTANVKSYFSNHDESL"
                + "KKNDRFIASVTDSENTNQREAASHGFGKTSGNSFKVNSCKDHIGKSMPNVLEDEVYETVV"
                + "DTSEEDSFSLCFSKCRTKNLQKVRTSKTRKKIFHEANADECEKSKNQVKEKYSFVSEVEP"
                + "NDTDPLDSNVANQKPFESGSDKISKEVVPSLACEWSQLTLSGLNGAQMEKIPLLHISSCD"
                + "QNISEKDLLDTENKRKKDFLTSENSLPRISSLPKSEKPLNEETVVNKRDEEQHLESHTDC"
                + "ILAVKQAISGTSPVASSFQGIKKSIFRIRESPKETFNASFSGHMTDPNFKKETEASESGL"
                + "EIHTVCSQKEDSLCPNLIDNGSWPATTTQNSVALKNAGLISTLKKKTNKFIYAIHDETSY"
                + "KGKKIPKDQKSELINCSAQFEANAFEAPLTFANADSGLLHSSVKRSCSQNDSEEPTLSLT"
                + "SSFGTILRKCSRNETCSNNTVISQDLDYKEAKCNKEKLQLFITPEADSLSCLQEGQCEND"
                + "PKSKKVSDIKEEVLAAACHPVQHSKVEYSDTDFQSQKSLLYDHENASTLILTPTSKDVLS"
                + "NLVMISRGKESYKMSDKLKGNNYESDVELTKNIPMEKNQDVCALNENYKNVELLPPEKYM"
                + "RVASPSRKVQFNQNTNLRVIQKNQEETTSISKITVNPDSEELFSDNENNFVFQVANERNN"
                + "LALGNTKELHETDLTCVNEPIFKNSTMVLYGDTGDKQATQVSIKKDLVYVLAEENKNSVK"
                + "QHIKMTLGQDLKSDISLNIDKIPEKNNDYMNKWAGLLGPISNHSFGGSFRTASNKEIKLS"
                + "EHNIKKSKMFFKDIEEQYPTSLACVEIVNTLALDNQKKLSKPQSINTVSAHLQSSVVVSD"
                + "CKNSHITPQMLFSKQDFNSNHNLTPSQKAEITELSTILEESGSQFEFTQFRKPSYILQKS"
                + "TFEVPENQMTILKTTSEECRDADLHVIMNAPSIGQVDSSKQFEGTVEIKRKFAGLLKNDC"
                + "NKSASGYLTDENEVGFRGFYSAHGTKLNVSTEALQKAVKLFSDIENISEETSAEVHPISL"
                + "SSSKCHDSVVSMFKIENHNDKTVSEKNNKCQLILQNNIEMTTGTFVEEITENYKRNTENE"
                + "DNKYTAASRNSHNLEFDGSDSSKNDTVCIHKDETDLLFTDQHNICLKLSGQFMKEGNTQI"
                + "KEDLSDLTFLEVAKAQEACHGNTSNKEQLTATKTEQNIKDFETSDTFFQTASGKNISVAK"
                + "ESFNKIVNFFDQKPEELHNFSLNSELHSDIRKNKMDILSYEETDIVKHKILKESVPVGTG"
                + "NQLVTFQGQPERDEKIKEPTLLGFHTASGKKVKIAKESLDKVKNLFDEKEQGTSEITSFS"
                + "HQWAKTLKYREACKDLELACETIEITAAPKCKEMQNSLNNDKNLVSIETVVPPKLLSDNL"
                + "CRQTENLKTSKSIFLKVKVHENVEKETAKSPATCYTNQSPYSVIENSALAFYTSCSRKTS"
                + "VSQTSLLEAKKWLREGIFDGQPERINTADYVGNYLYENNSNSTIAENDKNHLSEKQDTYL"
                + "SNSSMSNSYSYHSDEVYNDSGYLSKNKLDSGIEPVLKNVEDQKNTSFSKVISNVKDANAY"
                + "PQTVNEDICVEELVTSSSPCKNKNAAIKLSISNSNNFEVGPPAFRIASGKIVCVSHETIK"
                + "KVKDIFTDSFSKVIKENNENKSKICQTKIMAGCYEALDDSEDILHNSLDNDECSTHSHKV"
                + "FADIQSEEILQHNQNMSGLEKVSKISPCDVSLETSDICKCSIGKLHKSVSSANTCGIFST"
                + "ASGKSVQVSDASLQNARQVFSEIEDSTKQVFSKVLFKSNEHSDQLTREENTAIRTPEHLI"
                + "SQKGFSYNVVNSSAFSGFSTASGKQVSILESSLHKVKGVLEEFDLIRTEHSLHYSPTSRQ"
                + "NVSKILPRVDKRNPEHCVNSEMEKTCSKEFKLSNNLNVEGGSSENNHSIKVSPYLSQFQQ"
                + "DKQQLVLGTKVSLVENIHVLGKEQASPKNVKMEIGKTETFSDVPVKTNIEVCSTYSKDSE"
                + "NYFETEAVEIAKAFMEDDELTDSKLPSHATHSLFTCPENEEMVLSNSRIGKRRGEPLILV"
                + "GEPSIKRNLLNEFDRIIENQEKSLKASKSTPDGTIKDRRLFMHHVSLEPITCVPFRTTKE"
                + "RQEIQNPNFTAPGQEFLSKSHLYEHLTLEKSSSNLAVSGHPFYQVSATRNEKMRHLITTG"
                + "RPTKVFVPPFKTKSHFHRVEQCVRNINLEENRQKQNIDGHGSDDSKNKINDNEIHQFNKN"
                + "NSNQAAAVTFTKCEEEPLDLITSLQNARDIQDMRIKKKQRQRVFPQPGSLYLAKTSTLPR"
                + "ISLKAAVGGQVPSACSHKQLYTYGVSKHCIKINSKNAESFQFHTEDYFGKESLWTGKGIQ"
                + "LADGGWLIPSNDGKAGKEEFYRALCDTPGVDPKLISRIWVYNHYRWIIWKLAAMECAFPK"
                + "EFANRCLSPERVLLQLKYRYDTEIDRSRRSAIKKIMERDDTAAKTLVLCVSDIISLSANI"
                + "SETSSNKTSSADTQKVAIIELTDGWYAVKAQLDPPLLAVLKNGRLTVGQKIILHGAELVG"
                + "SPDACTPLEAPESLMLKISANSTRPARWYTKLGFFPDPRPFPLPLSSLFSDGGNVGCVDV"
                + "IIQRAYPIQWMEKTSSGLYIFRNEREEEKEAAKYVEAQQKRLEALFTKIQEEFEEHEENT"
                + "TKPYLPSRALTRQQVRALQDGAELYEAVKNAADPAYLEGYFSEEQLRALNNHRQMLNDKK"
                + "QAQIQLEIRKAMESAEQKEQGLSRDVTTVWKLRIVSYSKKEKDSVILSIWRPSSDLYSLL"
                + "TEGKRYRIYHLATSKSKSKSERANIQLAATKKTQYQQLPVSDEILFQIYQPREPLHFSKF"
                + "LDPDFQPSCSEVDLIGFVVSVVKKTGLAPFVYLSDECYNLLAIKFWIDLNEDIIKPHMLI"
                + "AASNLQWRPESKSGLLTLFAGDFSVFSASPKEGHFQETFNKMKNTVENIDILCNEAENKL"
                + "MHILHANDPKWSTPTKDCTSGPYTAQIIPGTGNKLLMSSPNCEIYYQSPLSLCMAKRKSV"
                + "STPVSAQMTSKSCKGEKEIDDQKNCKKRRALDFLSRLPLPPPVSPICTFVSPAAQKAFQP"
                + "PRSCGTKYETPIKKKELNSPQMTPFKKFNEISLLESNSIADEELALINTQALLSGSTGEK"
                + "QFISVSESTRTAPTSSEDYLRLKRRCTTSLIKEQESSQASTEECEKNKQDTITTKKYI");

        GapPenalty penalty = new SimpleGapPenalty();
        int gOpen = 8;
        int gExtend = 1;
        penalty.setOpenPenalty(gOpen);
        penalty.setExtensionPenalty(gExtend);

        SubstitutionMatrix<AminoAcidCompound> matrix = SubstitutionMatrixHelper.getBlosum62();

        NeedlemanWunsch nw = new NeedlemanWunsch(brca2Pat, brca2Uniprot, penalty, matrix);

        return nw;
    }

    /**
     * It retrieves the sequence from the Unitprot database given the ID
     *
     * @param uniProtId
     * @return
     * @throws Exception
     */
    private static ProteinSequence getSequenceForId(String uniProtId) throws Exception {
        URL uniprotFasta = new URL(String.format("http://www.uniprot.org/uniprot/%s.fasta", uniProtId));
        ProteinSequence seq = FastaReaderHelper.readFastaProteinSequence(uniprotFasta.openStream()).get(uniProtId);
        System.out.printf("id : %s %s%s%s", uniProtId, seq, System.getProperty("line.separator"), seq.getOriginalHeader());
        System.out.println();

        return seq;
    }

    public int updateResult(int idPatient, double avg) {
        int rowsAffected;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("UPDATE_RESULT"));) {
            pst.setDouble(1, avg);
            pst.setInt(2, idPatient);
            rowsAffected = pst.executeUpdate();
        } catch (SQLException e) {
            rowsAffected = 0;
        }

        return rowsAffected;
    }
}
