package com.mycompany.breastcancer.model.persist;

import com.mycompany.breastcancer.model.Admin;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author tarda
 */
public class AdminDAO {

    private static DBConnect dataSource;
    private final Properties queries;
    private static String PROPS_FILE;

    public AdminDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/admin_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));
        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    /**
     * It retrieves an admin from the database given the email
     *
     * @param email
     * @return
     */
    public Admin getAdminByEmail(String email) {
        Admin admin;

        try (Connection conn = dataSource.getConnection();
                PreparedStatement pst = conn.prepareStatement(getQuery("SELECT_EMAIL"));) {

            pst.setString(1, email);
            ResultSet res = pst.executeQuery();
            res.next();
            admin = new Admin(res.getInt("idAdmin"), res.getString("name"), res.getString("surname"), res.getString("email"), res.getString("password"));

        } catch (SQLException e) {
            admin = null;
        }

        return admin;
    }
}
