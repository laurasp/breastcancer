package com.mycompany.breastcancer.model;

/**
 *
 * @author tarda
 */
public class Patient {

    //attributes
    private int idPatient;
    private String name;
    private String surname;
    private String dni;
    private int age;
    private String email;
    private String telephone;
    private String password;
    private String brca1Seq;
    private String brca2Seq;
    private double result;
    private int idDoctor;

    //constructors
    public Patient() {
    }

    public Patient(int idPatient, String name, String surname, String dni, int age, String email, String telephone, String password, String brca1Seq, String brca2Seq, double result, int idDoctor) {
        this.idPatient = idPatient;
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
        this.password = password;
        this.brca1Seq = brca1Seq;
        this.brca2Seq = brca2Seq;
        this.result = result;
        this.idDoctor = idDoctor;
    }

    //login
    public Patient(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Patient(String brca1Seq) {
        this.brca1Seq = brca1Seq;
    }

    //update result
    public Patient(int idPatient, String brca1Seq, String brca2Seq, double result) {
        this.idPatient = idPatient;
        this.brca1Seq = brca1Seq;
        this.brca2Seq = brca2Seq;
        this.result = result;
    }
    
    //update and delete patient
    public Patient(int idPatient, String name) {
        this.idPatient = idPatient;
        this.name = name;
    }

    //add patient
    public Patient(String name, String surname, String dni, int age, String email, String telephone, String password, String brca1Seq, String brca2Seq, double result, int idDoctor) {
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
        this.password = password;
        this.brca1Seq = brca1Seq;
        this.brca2Seq = brca2Seq;
        this.result = result;
        this.idDoctor = idDoctor;
    }

    //list patient
    public Patient(int idPatient, String name, String surname, String dni, int age, String email, String telephone, double result, int idDoctor) {
        this.idPatient = idPatient;
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
        this.result = result;
        this.idDoctor = idDoctor;
    }

    //modify patient (admin)
    public Patient(int idPatient, String name, String surname, String dni, int age, String email, String telephone, int idDoctor) {
        this.idPatient = idPatient;
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
        this.idDoctor = idDoctor;
    }

    //modify patient (doctor)
    public Patient(int idPatient, String name, String surname, String dni, int age, String email, String telephone) {
        this.idPatient = idPatient;
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.age = age;
        this.email = email;
        this.telephone = telephone;
    }

    //getters and setters
    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBrca1Seq() {
        return brca1Seq;
    }

    public void setBrca1Seq(String brca1Seq) {
        this.brca1Seq = brca1Seq;
    }

    public String getBrca2Seq() {
        return brca2Seq;
    }

    public void setBrca2Seq(String brca2Seq) {
        this.brca2Seq = brca2Seq;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    //toString
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Patient{idPatient=").append(idPatient);
        sb.append(", name=").append(name);
        sb.append(", surname=").append(surname);
        sb.append(", dni=").append(dni);
        sb.append(", age=").append(age);
        sb.append(", email=").append(email);
        sb.append(", telephone=").append(telephone);
        sb.append(", password=").append(password);
        sb.append(", brca1Seq=").append(brca1Seq);
        sb.append(", brca2Seq=").append(brca2Seq);
        sb.append(", result=").append(result);
        sb.append(", idDoctor=").append(idDoctor);
        sb.append('}');
        return sb.toString();
    }
}
