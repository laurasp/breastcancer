package com.mycompany.breastcancer.model;

/**
 *
 * @author tarda
 */
public class Doctor {

    //attributes
    private int idDoctor;
    private String name;
    private String surname;
    private String email;
    private String telephone;
    private String password;

    //constructors
    public Doctor() {
    }

    public Doctor(int idDoctor, String name, String surname, String email, String telephone, String password) {
        this.idDoctor = idDoctor;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
        this.password = password;
    }

    //login
    public Doctor(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Doctor(String telephone) {
        this.telephone = telephone;
    }
    
    //update and delete doctor
    public Doctor(int idDoctor, String name) {
        this.idDoctor = idDoctor;
        this.name = name;
    }
    
    //add doctor
    public Doctor(String name, String surname, String email, String telephone, String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
        this.password = password;
    }

    //list doctor
    public Doctor(int idDoctor, String name, String surname, String email, String telephone) {
        this.idDoctor = idDoctor;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
    }
    
    //getters and setters
    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //toString
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Doctor{idDoctor=").append(idDoctor);
        sb.append(", name=").append(name);
        sb.append(", surname=").append(surname);
        sb.append(", email=").append(email);
        sb.append(", telephone=").append(telephone);
        sb.append(", password=").append(password);
        sb.append('}');
        return sb.toString();
    }
}
