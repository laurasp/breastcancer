package com.mycompany.breastcancer.model;

/**
 *
 * @author tarda
 */
public class Admin {

    //attributes
    private int idAdmin;
    private String name;
    private String surname;
    private String email;
    private String password;

    //constructors
    public Admin() {
    }

    public Admin(int idAdmin, String name, String surname, String email, String password) {
        this.idAdmin = idAdmin;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public Admin(String email, String password) {
        this.email = email;
        this.password = password;
    }

    //getters and setters
    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //toString
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Admin{idAdmin=").append(idAdmin);
        sb.append(", name=").append(name);
        sb.append(", surname=").append(surname);
        sb.append(", email=").append(email);
        sb.append(", password=").append(password);
        sb.append('}');
        return sb.toString();
    }
}
