package com.mycompany.breastcancer.controllers;

import com.mycompany.breastcancer.model.Doctor;
import com.mycompany.breastcancer.model.Patient;
import com.mycompany.breastcancer.model.User;
import com.mycompany.breastcancer.model.persist.DoctorDAO;
import com.mycompany.breastcancer.model.persist.PatientDAO;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.biojava.nbio.alignment.NeedlemanWunsch;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;

/**
 *
 * @author tarda
 */
@WebServlet(name = "patientController", urlPatterns = {"/patientController"})
public class patientController extends HttpServlet {

    private PatientDAO patDAO;
    private DoctorDAO docDAO;
    private String ruta;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.text.ParseException
     * @throws org.biojava.nbio.core.exceptions.CompoundNotFoundException
     */
    protected void doAction(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, ParseException, CompoundNotFoundException, Exception {
        String action = request.getParameter("actionPatient");
        ruta = getServletContext().getRealPath("/WEB-INF/resources");
        patDAO = new PatientDAO(ruta);
        docDAO = new DoctorDAO(ruta);

        // Security filter
        boolean role = request.getSession().getAttribute("logged_in") != null
                ? (Boolean) request.getSession().getAttribute("logged_in") : false;
        action = role ? action : null;

        if (action != null) {
            switch (action) {
                case "listPatientsByRole":
                    listPatientsByRole(request, response);
                    break;
                case "addPatientForm":
                    addPatientForm(request, response);
                    break;
                case "addNewPatientAdmin":
                    addNewPatientAdmin(request, response);
                    break;
                case "addNewPatientDoctor":
                    addNewPatientDoctor(request, response);
                    break;
                case "listPatientsToModify":
                    listPatientsToModify(request, response);
                    break;
                case "modifyPatientForm":
                    modifyPatientForm(request, response);
                    break;
                case "modifyPatientAdmin":
                    modifyPatientAdmin(request, response);
                    break;
                case "modifyPatientDoctor":
                    modifyPatientDoctor(request, response);
                    break;
                case "listPatientsToDelete":
                    listPatientsToDelete(request, response);
                    break;
                case "deletePatient":
                    deletePatient(request, response);
                    break;
                case "listPatientsToSequence":
                    listPatientsToSequence(request, response);
                    break;
                case "analyseSequencesAndUpdateResult":
                    analyseSequencesAndUpdateResult(request, response);
                    break;
                case "listPatientToViewResult":
                    listPatientToViewResult(request, response);
                    break;
                case "viewResults":
                    viewResults(request, response);
                    break;
                default:
                    break;
            }
        } else {
            request.setAttribute("error", "You have to be logged to see this function.");
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists a patient from the database given the email
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void listPatientToViewResult(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        String patEmail = (String) request.getSession().getAttribute("username");

        if (patEmail != null && !patEmail.isEmpty()) {
            Patient newPat = patDAO.getPatientByEmail(patEmail);

            if (newPat != null) {
                request.setAttribute("patient", newPat);
            } else {
                request.setAttribute("error", "There is not patients with that email");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("patient.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It analyses the BRCA1 and BRCA2 sequences and shows the result
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws CompoundNotFoundException
     */
    private void viewResults(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, CompoundNotFoundException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null) {
            String patient = request.getParameter("patient");
            String[] patientsParams = patient.split(";");
            String brca1Seq = patientsParams[0];
            String brca2Seq = patientsParams[1];
            NeedlemanWunsch nwBrca1 = patDAO.analyseBrca1(brca1Seq);
            NeedlemanWunsch nwBrca2 = patDAO.analyseBrca2(brca2Seq);

            SequencePair<ProteinSequence, AminoAcidCompound> alignmentBrca1 = nwBrca1.getPair();
            //double distanceBrca1 = nwBrca1.getDistance() * 100;
            double similarityBrca1 = nwBrca1.getSimilarity() * 100;

            SequencePair<ProteinSequence, AminoAcidCompound> alignmentBrca2 = nwBrca2.getPair();
            //double distanceBrca2 = nwBrca2.getDistance() * 100;
            double similarityBrca2 = nwBrca2.getSimilarity() * 100;

            double avg = (similarityBrca1 + similarityBrca2) / 2;
            String probability;
            if (avg <= 25) {
                probability = "very hight";
            } else if (avg <= 50) {
                probability = "hight";
            } else if (avg <= 75) {
                probability = "low";
            } else {
                probability = "very low";
            }

            //request.setAttribute("success", alignmentBrca1.toString(60));
            request.setAttribute("success", "Alignment:<br>" + alignmentBrca1.toString(117) + "<br>Similarity BRCA1: " + String.format("%.4f", similarityBrca1) + " %<br>Similarity BRCA2: " + String.format("%.4f", similarityBrca2) + " %<br>Average of similarity: " + String.format("%.4f", avg) + " %<br>Probability of breast cancer: " + probability);

            RequestDispatcher dispatcher = request.getRequestDispatcher("patient.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * If the user is ADMIN, the app will show all the patients; if user is a
     * DOCTOR, the app will list only the patients assigned to the logged doctor
     *
     * @param request
     * @param response
     */
    private void listPatientsByRole(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        // SENSITIVE INFORMATION =>  HIGH SECURITY
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : null;

        if (role != null) {
            if (role.equals(User.ADMIN.name())) {
                listAllPatients(request, response);
            } else if (role.equals(User.DOCTOR.name())) {
                listPatientsByDoctor(request, response);
            } else {
                request.setAttribute("error", "You don't have permission to list the patients.");
                response.sendRedirect("login.jsp");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists all the patients from the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void listAllPatients(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        String adminEmail = (String) request.getSession().getAttribute("username");

        if (adminEmail != null && !adminEmail.isEmpty()) {
            ArrayList<Patient> pats = patDAO.getAllPatients();

            if (!pats.isEmpty()) {
                request.setAttribute("patients", pats);
            } else {
                request.setAttribute("error", "There aren't any patients");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists the patients from the database assigned to the logged doctor
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void listPatientsByDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        String docEmail = (String) request.getSession().getAttribute("username");

        if (docEmail != null && !docEmail.isEmpty()) {
            ArrayList<Patient> pats = patDAO.getPatientsByDoctor(docEmail);

            if (!pats.isEmpty()) {
                request.setAttribute("patients", pats);
            } else {
                request.setAttribute("error", "There aren't any patients");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("doctor.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It shows the add patient form
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void addPatientForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : null;
        String page = "";

        if (role != null) {
            if (role.equals(User.ADMIN.name())) {
                page = "admin.jsp?showAddPatientForm";
            } else if (role.equals(User.DOCTOR.name())) {
                page = "doctor.jsp?showAddPatientForm";
            } else {
                request.setAttribute("error", "You don't have permission to add a patient.");
                response.sendRedirect("login.jsp");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It adds a new patient to the database. This function is only for admins.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ParseException
     */
    private void addNewPatientAdmin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null && role.equals(User.ADMIN.name())) {
            String adminEmail = (String) request.getSession().getAttribute("username");

            if (adminEmail != null && !adminEmail.isEmpty()) {
                String name = request.getParameter("patName");
                String surname = request.getParameter("patSurname");
                if (patDAO.getPatientByDni(request.getParameter("patDni")) == null) {
                    String dni = request.getParameter("patDni").toUpperCase();
                    int age = Integer.parseInt(request.getParameter("patAge"));
                    if (patDAO.getPatientByEmail(request.getParameter("patEmail")) == null) {
                        String email = request.getParameter("patEmail").toLowerCase();
                        if (patDAO.getPatientByTelephone(request.getParameter("patTelephone")) == null) {
                            String telephone = request.getParameter("patTelephone");
                            String password = (name.substring(0, 1) + surname).toLowerCase();
                            String brca1Seq = request.getParameter("brca1Seq").toUpperCase();
                            String brca2Seq = request.getParameter("brca2Seq").toUpperCase();
                            double result = 0;
                            int idDoctor = Integer.parseInt(request.getParameter("patDoctor"));

                            Patient newPatient = new Patient(name, surname, dni, age, email, telephone, password, brca1Seq, brca2Seq, result, idDoctor);
                            int rowsAffected = patDAO.addNewPatient(newPatient);

                            if (rowsAffected == 1) {
                                request.setAttribute("success", "Patient " + name + " was successfully added");
                            } else {
                                request.setAttribute("error", "Patient " + name + " could not be added");
                            }

                            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
                            dispatcher.forward(request, response);
                        } else {
                            response.sendRedirect("admin.jsp?showAddPatientForm&errorTelephone=true");
                        }
                    } else {
                        response.sendRedirect("admin.jsp?showAddPatientForm&errorEmail=true");
                    }
                } else {
                    response.sendRedirect("admin.jsp?showAddPatientForm&errorDni=true");
                }
            } else {
                response.sendRedirect("login.jsp");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It adds a new patient to the database. This function is only for doctors.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ParseException
     */
    private void addNewPatientDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null && role.equals(User.DOCTOR.name())) {
            String docEmail = (String) request.getSession().getAttribute("username");

            if (docEmail != null && !docEmail.isEmpty()) {
                String name = request.getParameter("patName");
                String surname = request.getParameter("patSurname");
                if (patDAO.getPatientByDni(request.getParameter("patDni")) == null) {
                    String dni = request.getParameter("patDni").toUpperCase();
                    int age = Integer.parseInt(request.getParameter("patAge"));
                    if (patDAO.getPatientByEmail(request.getParameter("patEmail")) == null) {
                        String email = request.getParameter("patEmail").toLowerCase();
                        if (patDAO.getPatientByTelephone(request.getParameter("patTelephone")) == null) {
                            String telephone = request.getParameter("patTelephone");
                            String password = (name.substring(0, 1) + surname).toLowerCase();
                            String brca1Seq = request.getParameter("brca1Seq").toUpperCase();
                            String brca2Seq = request.getParameter("brca2Seq").toUpperCase();
                            double result = 0;
                            Doctor newDoc = docDAO.getDoctorByEmail(docEmail);
                            int idDoctor = newDoc.getIdDoctor();

                            Patient newPatient = new Patient(name, surname, dni, age, email, telephone, password, brca1Seq, brca2Seq, result, idDoctor);
                            int rowsAffected = patDAO.addNewPatient(newPatient);

                            if (rowsAffected == 1) {
                                request.setAttribute("success", "Patient " + name + " was successfully added");
                            } else {
                                request.setAttribute("error", "Patient " + name + " could not be added");
                            }

                            RequestDispatcher dispatcher = request.getRequestDispatcher("doctor.jsp");
                            dispatcher.forward(request, response);
                        } else {
                            response.sendRedirect("doctor.jsp?showAddPatientForm&errorTelephone=true");
                        }
                    } else {
                        response.sendRedirect("doctor.jsp?showAddPatientForm&errorEmail=true");
                    }
                } else {
                    response.sendRedirect("doctor.jsp?showAddPatientForm&errorDni=true");
                }
            } else {
                response.sendRedirect("login.jsp");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * ADMIN user can edit all patients, DOCTOR user only the patients assigned
     * to the logged Doctor.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listPatientsToModify(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        listPatientsByRole(request, response);
    }

    /**
     * It shows the modify patient form
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void modifyPatientForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null) {
            String patient = request.getParameter("patient");
            String[] patientParams = patient.split(";");
            Patient newPat;
            String page;

            if (role.equals(User.ADMIN.name())) {
                newPat = new Patient(Integer.parseInt(patientParams[0]), patientParams[1], patientParams[2], patientParams[3], Integer.parseInt(patientParams[4]), patientParams[5], patientParams[6], Integer.parseInt(patientParams[7]));
                page = "admin.jsp";
            } else {
                newPat = new Patient(Integer.parseInt(patientParams[0]), patientParams[1], patientParams[2], patientParams[3], Integer.parseInt(patientParams[4]), patientParams[5], patientParams[6]);
                page = "doctor.jsp";
            }

            request.setAttribute("patientmodify", newPat);

            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It modifies a patient from the database. This function is only for admins
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void modifyPatientAdmin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null) {
            String adminEmail = (String) request.getSession().getAttribute("username");

            if (adminEmail != null && !adminEmail.isEmpty()) {
                int idPatient = Integer.parseInt(request.getParameter("patId"));
                String name = request.getParameter("patName");
                String surname = request.getParameter("patSurname");
                if (patDAO.getPatientByDni(request.getParameter("patDni")) == null) {
                    String dni = request.getParameter("patDni").toUpperCase();
                    int age = Integer.parseInt(request.getParameter("patAge"));
                    if (patDAO.getPatientByEmail(request.getParameter("patEmail")) == null) {
                        String email = request.getParameter("patEmail").toLowerCase();
                        if (patDAO.getPatientByTelephone(request.getParameter("patTelephone")) == null) {
                            String telephone = request.getParameter("patTelephone");
                            int idDoctor = Integer.parseInt(request.getParameter("patDoctor"));
                            Patient newPat = new Patient(idPatient, name, surname, dni, age, email, telephone, idDoctor);
                            int rowsAffected = patDAO.modifyPatientA(newPat);

                            if (rowsAffected == 1) {
                                request.setAttribute("success", "Patient " + newPat.getName() + " was successfully modified");
                            } else {
                                request.setAttribute("error", "Patient " + newPat.getName() + " could not be modified");
                            }

                            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
                            dispatcher.forward(request, response);
                        } else {
                            response.sendRedirect("modifyPatientFormA.jsp?errorTelephone=true");
                        }
                    } else {
                        response.sendRedirect("modifyPatientFormA.jsp?errorEmail=true");
                    }
                } else {
                    response.sendRedirect("modifyPatientFormA.jsp?errorDni=true");
                }
            } else {
                response.sendRedirect("login.jsp");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It modifies a patient from the database. This function is only for
     * doctors
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void modifyPatientDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null) {
            String docEmail = (String) request.getSession().getAttribute("username");

            if (docEmail != null && !docEmail.isEmpty()) {
                int idPatient = Integer.parseInt(request.getParameter("patId"));
                String name = request.getParameter("patName");
                String surname = request.getParameter("patSurname");
                if (patDAO.getPatientByDni(request.getParameter("patDni")) == null) {
                    String dni = request.getParameter("patDni").toUpperCase();
                    int age = Integer.parseInt(request.getParameter("patAge"));
                    if (patDAO.getPatientByEmail(request.getParameter("patEmail")) == null) {
                        String email = request.getParameter("patEmail").toLowerCase();
                        if (patDAO.getPatientByTelephone(request.getParameter("patTelephone")) == null) {
                            String telephone = request.getParameter("patTelephone");
                            Patient newPat = new Patient(idPatient, name, surname, dni, age, email, telephone);
                            int rowsAffected = patDAO.modifyPatientD(newPat);

                            if (rowsAffected == 1) {
                                request.setAttribute("success", "Patient " + newPat.getName() + " was successfully modified");
                            } else {
                                request.setAttribute("error", "Patient " + newPat.getName() + " could not be modified");
                            }

                            RequestDispatcher dispatcher = request.getRequestDispatcher("doctor.jsp");
                            dispatcher.forward(request, response);
                        } else {
                            response.sendRedirect("modifyPatientFormD.jsp?errorTelephone=true");
                        }
                    } else {
                        response.sendRedirect("modifyPatientFormD.jsp?errorEmail=true");
                    }
                } else {
                    response.sendRedirect("modifyPatientFormD.jsp?errorDni=true");
                }
            } else {
                response.sendRedirect("login.jsp");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * ADMIN user can delete all patients, DOCTOR user only the patients
     * assigned to the logged Doctor.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listPatientsToDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        listPatientsByRole(request, response);
    }

    /**
     * It deletes a patient from the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void deletePatient(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";
        String page = "";

        if (role != null) {
            String patient = request.getParameter("patient");
            String[] patientsParams = patient.split(";");
            int idPatient = Integer.parseInt(patientsParams[0]);
            String name = patientsParams[1];
            int rowsAffected = patDAO.deletePatient(idPatient);

            if (rowsAffected == 1) {
                request.setAttribute("success", "Patient " + name + " was successfully deleted");
            } else {
                request.setAttribute("error", "Patient " + name + " could not be deleted");
            }

            if (role.equals(User.ADMIN.name())) {
                page = "admin.jsp";
            } else if (role.equals(User.DOCTOR.name())) {
                page = "doctor.jsp";
            } else {
                request.setAttribute("error", "You don't have permission to modify a patient.");
                response.sendRedirect("login.jsp");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It list the patients of the logged doctor with the button to analyse the
     * sequence. This function is only for doctors, not for admins
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void listPatientsToSequence(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        listPatientsByRole(request, response);
    }

    /**
     * It analyses the BRCA1 and BRCA2 sequences, shows the result and update it
     * in the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     * @throws CompoundNotFoundException
     * @throws Exception
     */
    private void analyseSequencesAndUpdateResult(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, CompoundNotFoundException {
        String role = request.getSession().getAttribute("user_role") != null
                ? (String) request.getSession().getAttribute("user_role") : "";

        if (role != null) {
            String patient = request.getParameter("patient");
            String[] patientsParams = patient.split(";");
            int idPatient = Integer.parseInt(patientsParams[0]);
            String brca1Seq = patientsParams[1];
            String brca2Seq = patientsParams[2];
            NeedlemanWunsch nwBrca1 = patDAO.analyseBrca1(brca1Seq);
            NeedlemanWunsch nwBrca2 = patDAO.analyseBrca2(brca2Seq);

            SequencePair<ProteinSequence, AminoAcidCompound> alignmentBrca1 = nwBrca1.getPair();
            //double distanceBrca1 = nwBrca1.getDistance() * 100;
            double similarityBrca1 = nwBrca1.getSimilarity() * 100;

            SequencePair<ProteinSequence, AminoAcidCompound> alignmentBrca2 = nwBrca2.getPair();
            //double distanceBrca2 = nwBrca2.getDistance() * 100;
            double similarityBrca2 = nwBrca2.getSimilarity() * 100;

            double avg = (similarityBrca1 + similarityBrca2) / 2;
            String probability;
            if (avg <= 25) {
                probability = "very hight";
            } else if (avg <= 50) {
                probability = "hight";
            } else if (avg <= 75) {
                probability = "low";
            } else {
                probability = "very low";
            }

            int rowsAffected = patDAO.updateResult(idPatient, avg);

            if (rowsAffected == 1) {
                request.setAttribute("success", "Result was successfully updated");
            } else {
                request.setAttribute("error", "Result could not be updated");
            }

            request.setAttribute("success", "Alignment:<br>" + alignmentBrca1.toString(117) + "<br>Similarity BRCA1: " + String.format("%.4f", similarityBrca1) + " %<br>Similarity BRCA2: " + String.format("%.4f", similarityBrca2) + " %<br>Average of similarity: " + String.format("%.4f", avg) + " %<br>Probability of breast cancer: " + probability);

            RequestDispatcher dispatcher = request.getRequestDispatcher("doctor.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            doAction(request, response);
        } catch (Exception ex) {
            Logger.getLogger(patientController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            doAction(request, response);
        } catch (Exception ex) {
            Logger.getLogger(patientController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
