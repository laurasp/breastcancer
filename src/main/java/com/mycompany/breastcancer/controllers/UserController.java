package com.mycompany.breastcancer.controllers;

import com.mycompany.breastcancer.model.Admin;
import com.mycompany.breastcancer.model.Doctor;
import com.mycompany.breastcancer.model.Patient;
import com.mycompany.breastcancer.model.User;
import com.mycompany.breastcancer.model.persist.AdminDAO;
import com.mycompany.breastcancer.model.persist.DoctorDAO;
import com.mycompany.breastcancer.model.persist.PatientDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tarda
 */
@WebServlet(name = "UserController", urlPatterns = {"/user"})
public class UserController extends HttpServlet {

    private String path;
    private DoctorDAO docDAO;
    private PatientDAO patDAO;
    private AdminDAO adminDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        path = getServletContext().getRealPath("/WEB-INF/resources");
        docDAO = new DoctorDAO(path);
        patDAO = new PatientDAO(path);
        adminDAO = new AdminDAO(path);

        if (request.getParameter("action") != null) {
            String action = request.getParameter("action");
            switch (action) {
                case "login":
                    login(request, response);
                    break;
                case "logout":
                    logout(request, response);
                    break;
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * Logs in an existing user by its email and password.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // Recogemos desde el formulario
        String email = request.getParameter("username").toLowerCase();
        String password = request.getParameter("password");

        HttpSession session = request.getSession(true);
        if (patDAO.getPatientByEmail(email) != null) {
            Patient pat = patDAO.getPatientByEmail(email);
            if (pat.getPassword().equals(password)) {
                session.setAttribute("logged_in", true);
                session.setAttribute("user_role", User.PATIENT.toString());
                session.setAttribute("username", pat.getEmail());
                response.sendRedirect("patient.jsp");
            } else {
                response.sendRedirect("login.jsp?error=true");
            }
        } else if (docDAO.getDoctorByEmail(email) != null) {
            Doctor doc = docDAO.getDoctorByEmail(email);
            if (doc.getPassword().equals(password)) {
                session.setAttribute("logged_in", true);
                session.setAttribute("user_role", User.DOCTOR.toString());
                session.setAttribute("username", doc.getEmail());
                response.sendRedirect("doctor.jsp");
            } else {
                response.sendRedirect("login.jsp?error=true");
            }
        } else if (adminDAO.getAdminByEmail(email) != null) {
            Admin admin = adminDAO.getAdminByEmail(email);
            if (admin.getPassword().equals(password)) {
                session.setAttribute("logged_in", true);
                session.setAttribute("user_role", User.ADMIN.toString());
                session.setAttribute("username", admin.getEmail());
                response.sendRedirect("admin.jsp");
            } else {
                response.sendRedirect("login.jsp?error=true");
            }
        } else {
            response.sendRedirect("login.jsp?error=true");
        }
    }

    /**
     * Logs out the current active user.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        session.removeAttribute("logged_in");
        session.removeAttribute("user_role");
        session.removeAttribute("username");
        session.invalidate();
        response.sendRedirect("login.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
