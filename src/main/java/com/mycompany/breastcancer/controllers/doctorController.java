package com.mycompany.breastcancer.controllers;

import com.mycompany.breastcancer.model.Doctor;
import com.mycompany.breastcancer.model.persist.DoctorDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tarda
 */
@WebServlet(name = "doctorController", urlPatterns = {"/doctorController"})
public class doctorController extends HttpServlet {

    private DoctorDAO docDAO;
    private String ruta;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doAction(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("actionDoctor");
        ruta = getServletContext().getRealPath("/WEB-INF/resources");
        docDAO = new DoctorDAO(ruta);

        // Security filter
        boolean role = request.getSession().getAttribute("logged_in") != null
                ? (Boolean) request.getSession().getAttribute("logged_in") : false;
        action = role ? action : null;

        if (action != null) {
            switch (action) {
                case "listAllDoctors":
                    listAllDoctors(request, response);
                    break;
                case "addDoctorForm":
                    response.sendRedirect("admin.jsp?showAddDoctorForm");
                    break;
                case "addNewDoctor":
                    addNewDoctor(request, response);
                    break;
                case "listDoctorsToModify":
                    listDoctorsToModify(request, response);
                    break;
                case "modifyDoctorForm":
                    modifyDoctorForm(request, response);
                    break;
                case "modifyDoctor":
                    modifyDoctor(request, response);
                    break;
                case "listDoctorsToDelete":
                    listDoctorsToDelete(request, response);
                    break;
                case "deleteDoctor":
                    deleteDoctor(request, response);
                    break;
                default:
                    response.sendRedirect("admin.jsp");
                    break;
            }
        } else {
            request.setAttribute("error", "You have to be logged as admin to see this function.");
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists all the doctors from the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listAllDoctors(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String adminUsername = (String) request.getSession().getAttribute("username");

        if (adminUsername != null && !adminUsername.isEmpty()) {
            ArrayList<Doctor> docs = docDAO.getAllDoctors();

            if (!docs.isEmpty()) {
                request.setAttribute("doctors", docs);
            } else {
                request.setAttribute("error", "There aren't any doctors");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It adds a new doctor to the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void addNewDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String adminUsername = (String) request.getSession().getAttribute("username");

        if (adminUsername != null && !adminUsername.isEmpty()) {
            String name = request.getParameter("docName");
            String surname = request.getParameter("docSurname");
            if (docDAO.getDoctorByEmail(request.getParameter("docEmail")) == null) {
                String email = request.getParameter("docEmail").toLowerCase();
                if (docDAO.getDoctorByTelephone(request.getParameter("docTelephone")) == null) {
                    String telephone = request.getParameter("docTelephone");
                    String password = (name.substring(0, 1) + surname).toLowerCase();

                    Doctor newDoc = new Doctor(name, surname, email, telephone, password);
                    int rowsAffected = docDAO.addNewDoctor(newDoc);

                    if (rowsAffected == 1) {
                        request.setAttribute("success", "Doctor " + name + " was successfully added");
                    } else {
                        request.setAttribute("error", "Doctor " + name + " could not be added");
                    }

                    RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
                    dispatcher.forward(request, response);
                } else {
                    response.sendRedirect("admin.jsp?showAddDoctorForm&errorTelephone=true");
                }
            } else {
                response.sendRedirect("admin.jsp?showAddDoctorForm&errorEmail=true");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists all the doctors with the modify button
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listDoctorsToModify(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        listAllDoctors(request, response);
    }

    /**
     * It shows the modify doctor form
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void modifyDoctorForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String adminUsername = (String) request.getSession().getAttribute("username");

        if (adminUsername != null && !adminUsername.isEmpty()) {
            String doctor = request.getParameter("doctor");
            String[] doctorParams = doctor.split(";");
            Doctor newDoc = new Doctor(Integer.parseInt(doctorParams[0]), doctorParams[1], doctorParams[2], doctorParams[3], doctorParams[4]);
            request.setAttribute("doctormodify", newDoc);
            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It modifies a doctor from the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void modifyDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String adminUsername = (String) request.getSession().getAttribute("username");

        if (adminUsername != null && !adminUsername.isEmpty()) {
            int idDoctor = Integer.parseInt(request.getParameter("docId"));
            String name = request.getParameter("docName");
            String surname = request.getParameter("docSurname");
            if (docDAO.getDoctorByEmail(request.getParameter("docEmail")) == null) {
                String email = request.getParameter("docEmail").toLowerCase();
                if (docDAO.getDoctorByTelephone(request.getParameter("docTelephone")) == null) {
                    String telephone = request.getParameter("docTelephone");

                    Doctor newDoc = new Doctor(idDoctor, name, surname, email, telephone);
                    int rowsAffected = docDAO.modifyDoctor(newDoc);

                    if (rowsAffected == 1) {
                        request.setAttribute("success", "Doctor " + newDoc.getName() + " was successfully modified");
                    } else {
                        request.setAttribute("error", "Doctor " + newDoc.getName() + " could not be modified");
                    }

                    RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
                    dispatcher.forward(request, response);
                } else {
                    response.sendRedirect("modifyDoctorForm.jsp?errorTelephone=true");
                }
            } else {
                response.sendRedirect("modifyDoctorForm.jsp?errorEmail=true");
            }
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    /**
     * It lists all the doctors with the delete button
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listDoctorsToDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        listAllDoctors(request, response);
    }

    /**
     * It deletes a doctor from the database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void deleteDoctor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String adminUsername = (String) request.getSession().getAttribute("username");

        if (adminUsername != null && !adminUsername.isEmpty()) {
            String doctor = request.getParameter("doctor");
            String[] doctorParams = doctor.split(";");
            int idDoctor = Integer.parseInt(doctorParams[0]);
            String name = doctorParams[1];
            int rowsAffected = docDAO.deleteDoctor(idDoctor);

            if (rowsAffected == 1) {
                request.setAttribute("success", "Doctor " + name + " was successfully deleted");
            } else {
                request.setAttribute("error", "Doctor " + name + " could not be deleted");
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect("login.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doAction(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doAction(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
