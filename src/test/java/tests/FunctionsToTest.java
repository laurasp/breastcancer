package tests;

/**
 *
 * @author tarda
 */
public class FunctionsToTest {

    public FunctionsToTest() {
    }

    //This method checks if a field is empty
    public boolean notEmpty(String field) {
        return !field.equals("") && !field.isEmpty();
    }
    
    //This method checks if a field is null
    public boolean notNull(String field) {
        return field != null;
    }

    //This method checks if the length of a field exceeds a given size
    public boolean maxLength(String field, int maxLength) {
        return field.length() < maxLength;
    }

    //This method checks if the age is greater than 0 and less than 150
    public boolean age(int age) {
        return age > 0 && age < 150;
    }

    //This method checks if the email has a valid format
    public boolean emailFormat(String email) {
        String pattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
        return email.toLowerCase().matches(pattern);
    }

    //This method checks if the DNI has a valid format (8 numbers + 1 letter)
    public boolean dniFormat(String DNI) {
        String pattern = "^[0-9]{8}[A-Z]$";
        return DNI.toUpperCase().matches(pattern);
    }

    //This method checks if the letter of the DNI corresponds to the %23 of the 8 numbers
    public boolean dniLetter(String DNI) {
        String letters = "TRWAGMYFPDXBNJZSQVHLCKE";
        String regex = "[A-Z]";
        String number_str = DNI.split(regex)[0];
        int number = Integer.parseInt(number_str);
        char letter_input = DNI.toUpperCase().charAt(number_str.length());
        char letter_calc = letters.charAt(number % 23);
        return letter_input == letter_calc;
    }

    //This method checks if the complete DNI is valid (2 previous methods at once)
    public boolean validDni(String DNI) {
        return dniFormat(DNI) && dniLetter(DNI);
    }

    //This method checks if the protein sequence is valid
    public boolean validSequence(String sequence) {
        String pattern = "^[ARNDCQEGHILKMFPSTWYV]+$";
        return sequence.toUpperCase().matches(pattern);
    }
}
