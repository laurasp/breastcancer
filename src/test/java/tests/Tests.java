package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author tarda
 */
public class Tests {

    FunctionsToTest f = new FunctionsToTest();

    //Test that a certain field is not empty
    @Test
    public void testNotEmpty() {
        System.out.println("Test not empty");
        String field = "";
        boolean expResult = false;
        boolean result = f.notEmpty(field);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test not empty failed");
        }
    }

    //Test that a certain field is not null
    @Test
    public void testNotNull() {
        System.out.println("Test not null");
        String field = null;
        boolean expResult = false;
        boolean result = f.notNull(field);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test not null failed");
        }
    }

    //Test that the length of a certain field is shorter than a given value
    @Test
    public void testMaxLength() {
        System.out.println("Test max length");
        String field = "holaquetaladiosholaquetaladios";
        int maxLength = 30;
        boolean expResult = false;
        boolean result = f.maxLength(field, maxLength);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test max length failed");
        }
    }

    //Test that the age is greater than 0 and less than 150
    @Test
    public void testAge() {
        System.out.println("Test age");
        int age = 50;
        boolean expResult = true;
        boolean result = f.age(age);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test age failed");
        }
    }

    //Test that the email has a valid format
    @Test
    public void testEmailFormat() {
        System.out.println("Test email format");
        String email = "lsimon@gmail.com";
        boolean expResult = true;
        boolean result = f.emailFormat(email);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test email format failed");
        }
    }

    //Test that the DNI has a valid format (8 numbers + 1 letter)
    @Test
    public void testDniFormat() {
        System.out.println("Test DNI format");
        String DNI = "1234567A";
        boolean expResult = false;
        boolean result = f.emailFormat(DNI);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test DNI format failed");
        }
    }

    //Test that the letter of the DNI corresponds to the the %23 of the 8 numbers
    @Test
    public void testDniLetter() {
        System.out.println("Test DNI letter");
        String DNI = "23T";
        boolean expResult = true;
        boolean result = f.dniLetter(DNI);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test DNI letter failed");
        }
    }

    //Test that the complete DNI is valid (the previous 2 methods at once)
    @Test
    public void testValidDni() {
        System.out.println("Test valid DNI");
        String DNI = "71941487X";
        boolean expResult = false;
        boolean result = f.validDni(DNI);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test valid DNI failed");
        }
    }

    //Test that the protein sequence is valid
    @Test
    public void testValidSequence() {
        System.out.println("Test valid sequence");
        String sequence = "arndcarndcarndc";
        boolean expResult = true;
        boolean result = f.validSequence(sequence);
        assertEquals(expResult, result);
        if (expResult != result) {
            fail("Test valid sequence failed");
        }
    }
}
