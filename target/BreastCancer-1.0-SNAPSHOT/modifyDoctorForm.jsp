<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="html/adminMenu.html"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin</title>
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <form action="doctorController" method="POST">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Name:</label>
                    <input type="text" class="form-control" id="inputDoctor" value="${doctormodify.name}" name="docName" required maxlength="30">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Surname:</label>
                    <input type="text" class="form-control" id="inputDoctor" value="${doctormodify.surname}" name="docSurname" required malength="30">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Email:</label>
                    <input type="email" class="form-control" id="inputDoctor" value="${doctormodify.email}" name="docEmail" required maxlength="50" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                    <%
                        if (request.getParameter("errorEmail") != null) {
                            out.println("<p class='error'>Email already exists in database</p>");
                        }
                    %>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputDoctor">Telephone:</label>
                    <input type="text" class="form-control" id="inputDoctor" value="${doctormodify.telephone}" name="docTelephone" required maxlength="20">
                    <%
                        if (request.getParameter("errorTelephone") != null) {
                            out.println("<p class='error'>Telephone already exists in database</p>");
                        }
                    %>
                </div>
            </div>
            <input type="hidden" value="${doctormodify.idDoctor}" name="docId"/>
            <button type="submit" class="btn btn-primary" name="actionDoctor" value="modifyDoctor">Modify doctor</button>
        </form>
    </body>
</html>

<jsp:include page="html/footer.html"/>
