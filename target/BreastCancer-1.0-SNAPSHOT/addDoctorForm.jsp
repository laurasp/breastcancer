<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="doctorController" method="POST">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputDoctor">Name:</label>
            <input type="text" class="form-control" id="inputDoctor" name="docName" placeholder="Name" required maxlength="30">
        </div>
        <div class="form-group col">
            <label for="inputDoctor">Surname:</label>
            <input type="text" class="form-control" id="inputDoctor" name="docSurname" placeholder="Surname" required maxlength="30">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputDoctor">Email:</label>
            <input type="email" class="form-control" id="inputDoctor" name="docEmail" placeholder="Email" required maxlength="50" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
            <%
                if (request.getParameter("errorEmail") != null) {
                    out.println("<p class='error'>Email already exists in database</p>");
                }
            %>
        </div>
        <div class="form-group col-md-6">
            <label for="inputDoctor">Telephone:</label>
            <input type="text" class="form-control" id="inputDoctor" name="docTelephone" placeholder="Telephone" required maxlength="20">
            <%
                if (request.getParameter("errorTelephone") != null) {
                    out.println("<p class='error'>Telephone already exists in database</p>");
                }
            %>
        </div>
    </div>
    <input type="hidden" value="${doctormodify.id}" name="doctorId"/>
    <button type="submit" class="btn btn-primary" value="addNewDoctor" name="actionDoctor">Add doctor</button>
</form>
