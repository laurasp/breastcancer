<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<form action="patientController" method="POST">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputPatient">Name:</label>
            <input type="text" class="form-control" id="inputPatient" name="patName" placeholder="Name" required maxlength="30">
        </div>
        <div class="form-group col">
            <label for="inputPatient">Surname:</label>
            <input type="text" class="form-control" id="inputPatient" name="patSurname" placeholder="Surname" required maxlength="30">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputPatient">DNI:</label>
            <input type="text" class="form-control" id="inputPatient" name="patDni" placeholder="DNI" required maxlength="9" pattern="^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke]$">
            <%
                if (request.getParameter("errorDni") != null) {
                    out.println("<p class='error'>DNI already exists in database</p>");
                }
            %>
        </div>
        <div class="form-group col-md-6">
            <label for="inputPatient">Age:</label>
            <input type="number" class="form-control" id="inputPatient" name="patAge" placeholder="Age" required min="1" max="150">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputPatient">Email:</label>
            <input type="email" class="form-control" id="inputPatient" name="patEmail" placeholder="Email" required maxlength="50" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
            <%
                if (request.getParameter("errorEmail") != null) {
                    out.println("<p class='error'>Email already exists in database</p>");
                }
            %>
        </div>
        <div class="form-group col-md-6">
            <label for="inputPatient">Telephone:</label>
            <input type="text" class="form-control" id="inputPatient" name="patTelephone" placeholder="Telephone" required maxlength="20">
            <%
                if (request.getParameter("errorTelephone") != null) {
                    out.println("<p class='error'>Telephone already exists in database</p>");
                }
            %>
        </div>
    </div>
    <div class="form-row">   
        <div class="form-group col-md-6">
            <label for="inputPatient">BRCA1 protein sequence:</label>
            <input type="text" class="form-control" id="inputPatient" name="brca1Seq" placeholder="BRCA1 protein sequence" required pattern="^[ARNDCQEGHILKMFPSTWYVarndcqeghilkmfpstwyv]+$">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPatient">BRCA2 protein sequence:</label>
            <input type="text" class="form-control" id="inputPatient" name="brca2Seq" placeholder="BRCA2 protein sequence" required pattern="^[ARNDCQEGHILKMFPSTWYVarndcqeghilkmfpstwyv]+$">
        </div>
    </div>
    <div class="form-row">   
        <div class="form-group col-md-6">
            <label for="inputPatient">Doctor:</label>
            <select class="form-control" id="inputPatient" name="patDoctor" required>
                <option value="">--- Select one option ---</option>
                <option value="1">Diego Carreras</option>
                <option value="2">Paco Garcia</option>
            </select>
        </div>
    </div>
    <input type="hidden" value="${patientmodify.id}" name="patientId"/>
    <button type="submit" class="btn btn-primary" value="addNewPatientAdmin" name="actionPatient">Add patient</button>
</form>
