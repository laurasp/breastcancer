1) DIRECTORIES
- src
    * main:
        - java -> com -> mycompany -> breastcancer:
            * controllers -> admin, doctor, patient and user controllers
            * model:
                - admin, doctor, patient and user models
                - persist:
                    * connection to database
                    * admin, doctor and patient DAOs
        - webapp:
            FOLDERS:
            * META-INF -> context.xml
            * WEB-INF -> resources -> SQL queries to the database tables
            * css -> stylesheet
            * html:
                - admin and user manuals
                - admin, doctor and patient menus
                - footer
            * img:
                - logo image
                - admin and user manuals images
            * vendors -> bootstrap and jquery resources
            FILES .JSP:
            * forms to add and modify a patient
            * forms to add and modify a doctor
            * index.jsp -> redirects lo login.jsp
            * login.jsp
            * admin.jsp, doctor.jsp, patient.jsp -> admin, doctor and patient main pages
    * test -> java -> tests -> functions to test and tests
- README.md -> this file
- bd.sql -> SQL commands to create the user, the database and the tables
- pom.xml -> biojava, jstl, mysql and junit dependencies
- brca_sequences.txt -> BRCA1 and BRCA2 modified sequences to test

2) MAIN FUNCTION
Web Pages -> index.jsp -> it will redirect to the login page (login.jsp)
To authenticate as an admin -> Email: lsimon@gmail.com, password: lsimon
To authenticate as a doctor -> Email: dcarreras@gmail.com, password: dcarreras
To authenticate as a patient -> Email: iperez@gmail.com, password: iperez

3) INSTALLATION
- Install Java JDK 8
- Install Apache Tomcat 8.5
- Install MySQL
- Install phpMyAdmin
In phpMyAdmin -> execute the file bd.sql

4) COMPILATION
In Apache NetBeans ->   1. Clean and Build, to import all the dependencies and repositories from the file pom.xml
                        2. Execute Apache Tomcat
                        3. Run the project
